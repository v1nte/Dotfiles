//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" ", "sb-ram",		30,		0},
	{" ", "sb-BTC",		60,		0},
	{" ", "sb-volume",		0,		1},
	/* {"", "sb-bat",		10,		0}, */
	/* {"", "sb-isnet",		10,		0}, */
	{" ", "sb-date",		30,		0},
	{"      ", "",		0,		0}, // trayer space
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
