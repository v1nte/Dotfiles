# v1nte's dotfiles
Peronal Workflow :)

![desktop-image](.images/desktop.png)

## Software

* [DWM](https://dwm.suckless.org/) : Window Manager
* [Qtile](http://qtile.org/) : Tiling window manager for the X Window System
* [dmenu](https://tools.suckless.org/dmenu/): not just a program launcher
* [Rofi](https://wiki.archlinux.org/title/Rofi/): dmenu replacement
* [nvim](https://neovim.io/): Text editor
* [alacritty](https://alacritty.org/): Terminal Emulator
* [sxhkd:](https://github.com/baskerville/sxhkd) Simple X HotKey Daemon (keybinds manage)
* [(ohmy) ZSH](https://ohmyz.sh/): Shell

## Deploy
Soon...
## Why arch?

Pacman, AUR, and...
![mug](.images/mug.jpeg)
