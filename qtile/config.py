# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from sys import get_coroutine_origin_tracking_depth
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.dgroups import simple_key_binder

mod = "mod4"
terminal = "alacritty"
browser = "brave"
FONT = "Caskaydia Cove Nerd Font Mono"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch Browser"),
    Key([mod], "p", lazy.spawn('rofi -show run'), desc="Spawn a command using a prompt widget"),

    #######################
    ### Window controls ###
    #######################
    Key([mod], "j",
     lazy.layout.down(),
     desc='Move focus down in current stack pane'
     ),
    Key([mod], "k",
     lazy.layout.up(),
     desc='Move focus up in current stack pane'
     ),
    Key([mod, "shift"], "j",
     lazy.layout.shuffle_down(),
     lazy.layout.section_down(),
     desc='Move windows down in current stack'
     ),
    Key([mod, "shift"], "k",
     lazy.layout.shuffle_up(),
     lazy.layout.section_up(),
     desc='Move windows up in current stack'
     ),
    Key([mod], "h",
     lazy.layout.shrink(),
     lazy.layout.decrease_nmaster(),
     desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
     ),
    Key([mod], "l",
     lazy.layout.grow(),
     lazy.layout.increase_nmaster(),
     desc='Expand window (MonadTall), increase number in master pane (Tile)'
     ),

    Key([mod], "Tab", lazy.screen.toggle_group(), desc="Toggle to last visited group"),
    
    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),

    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

	# Volume
	Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer --decrease 2")),
	Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer --increase 2")),
	Key([], "XF86AudioMute", lazy.spawn("pamixer --toggle-mute")),

	# Brightness
	Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +5%")),
	Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 5%-")),

    # Screenshot
	Key([mod, "shift"], "s", lazy.spawn("/home/v1nte/.local/bin/ss.sh")),
]


groups = [Group("", layout='monadtall'), # Terminal
          Group("", layout='monadtall'), # Firefox
          Group("", layout='monadtall'), # Tux
          Group("", layout='monadtall'), # Vim
          Group("", layout='monadtall'), # Python
          Group("", layout='monadtall'), # C
          Group("", layout='monadtall'), # {}
          Group("", layout='monadtall'), # Arch
          Group("", layout='monadtall'),]# GitLab


layouts = [
    layout.Max(),
    layout.MonadTall(
        font = FONT,
        fontsize = 14,
        margin = 10,
        ),
]

widget_defaults = dict(
    font = FONT,
    fontsize = 14,
    padding = 4,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Image(filename="~/.config/qtile/arch-logo.png",scale=True,margin_x=6),
                widget.GroupBox(fontsize=24,margin_x=6),
                widget.Prompt(),
                widget.WindowName(fontsize=16),
                widget.Battery(format="{percent:2.0%}"),
                widget.BatteryIcon(),
                widget.Systray(),
                widget.Clock(format="%H:%M [GMT%Z] | (%a) %D "),
                #widget.QuickExit(),
            ],
            24,
            #border_width=[0, 0, 2, 0],  # Draw top and bottom borders
            #border_color=["ff00ff", "000000", "534666", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = simple_key_binder(mod)
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "I Use Arch, btw"
